package com.lee.dao;
/**
 * @author DYLAN(2102028233@qq.com)
 * @since 2019年9月13日 下午2:48:25
 * @description 数据访问层
 */

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Repository;

import com.lee.entity.User;
import com.lee.repository.UserRepository;

@Repository
public class UserDao {
	@Resource
	private UserRepository userRepository;
	
	public List<User> listByEmail(User user) {
		return userRepository.findByUserEmail(user.getUserEmail());
	}
	
	public List<User> listByEmailAndPassword(User user) {
		return userRepository.findByUserEmailAndUserPassword(user.getUserEmail(),user.getUserPassword());
	}

}
