package com.lee.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.lee.entity.User;

/**
 * @author DYLAN(2102028233@qq.com)
 * @since 2019年9月13日 下午2:46:21
 * @description 数据库操作接口 
 * Repository是最顶层的接口，是一个空类 <-- CrudRepository <-- PagingAndSortingRepository <-- JpaRepository <-- ExpandJpaRepository
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer>{
	List<User> findByUserEmail(String userEmail);
	List<User> findByUserEmailAndUserPassword(String userEmail, String userPassword);
}
