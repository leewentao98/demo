package com.lee.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * @author DYLAN(2102028233@qq.com)
 * @time 2019年9月2日 下午9:38:21
 * @description user
 *
 */
@Data
@Entity
@Table(name="user_account")
public class User {
//	设置id自增
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String userEmail;
	private String userPassword;
}
