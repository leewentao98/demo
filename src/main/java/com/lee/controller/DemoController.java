package com.lee.controller;
/**
 * @author DYLAN(2102028233@qq.com)
 * @time 2019年9月2日 下午2:42:34
 * @description test contoller
 * */
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;


@Controller
public class DemoController {
	@ResponseBody
	@RequestMapping("/hello")
	public String helloWorld() {
		return "hello Spring Boot";
	}
}
