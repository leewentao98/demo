package com.lee.controller;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.lee.entity.User;
import com.lee.service.UserService;
import com.lee.token.JWTAuth;
import com.lee.util.ResultUtil;
/**
 * @author DYLAN(2102028233@qq.com)
 * @time 2019年9月3日 下午12:05:01
 * @description 用于接收、处理用户模块发来的请求
 *
 */
@RestController
public class UserController {
	@Resource
	private UserService userService;
	
	@GetMapping("/getUser")
	public String userList() {
		return "OK";
	}
	
	@PostMapping("/user/login")
	public ResultUtil userLogin(@RequestBody User user) {
		System.out.println(user);
		System.out.println(JWTAuth.getToken(user));
		return userService.login(user);
	}
}
