package com.lee.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.lee.dao.UserDao;
import com.lee.entity.User;
import com.lee.util.ResultUtil;

/**
 * @author DYLAN(2102028233@qq.com)
 * @since 2019年9月13日 下午2:47:25
 * @description 服务层/业务层：负责处理业务逻辑，这里省去了Service的接口层，直接实现
 */
@Service
public class UserService {
	@Resource
	private UserDao userDao;

	ResultUtil result;

	public ResultUtil login(User user) {

		if (user.equals(null) || user.getUserEmail().equals("") || user.getUserPassword().equals("")) {
			return null;
		} else if (userDao.listByEmail(user).size() == 0) {
			return new ResultUtil(404, "用户不存在");
		} else {
			List<User> resultList = userDao.listByEmailAndPassword(user);
			if (resultList.size()==1) 
				return new ResultUtil(200, "登录成功", resultList.get(0));
			else
				return new ResultUtil(500, "账号或密码不正确");
		}
	}
}
