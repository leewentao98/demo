package com.lee.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.lee.entity.User;

public class JWTAuth {
	public static String getToken(User user) {
		String key = "imleeyu";
		String token = JWT.create().withAudience(user.getUserEmail()).sign(Algorithm.HMAC256(key));
		return token;
	}

}
