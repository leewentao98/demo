package com.lee.token;
/**
 * @author DYLAN(2102028233@qq.com)
 * @since 2019年9月18日 下午4:59:02
 * @description 注册拦截器
 */
import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

@Configuration
public class WebAppConfigure extends WebMvcConfigurationSupport{
	private List<String> excludePath;

	@Override
	protected void addInterceptors(InterceptorRegistry registry) {
		// 设置拦截的URL
		registry.addInterceptor(new AuthInterceptor()).addPathPatterns("/**").addPathPatterns(setExcludePath());
		super.addInterceptors(registry);
	}

	@Override
	protected void addResourceHandlers(ResourceHandlerRegistry registry) {
		// 配置静态资源
		super.addResourceHandlers(registry);
	}
	
//	设置不拦截的URL
	public List<String> setExcludePath() {
		this.excludePath = new ArrayList<>();
		excludePath.add("/user/login");
		return excludePath;
	}

}
