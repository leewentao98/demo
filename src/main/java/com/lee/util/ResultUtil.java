package com.lee.util;

import lombok.Getter;
import lombok.Setter;

/**
 * @author DYLAN(2102028233@qq.com)
 * @time 2019年9月3日 下午12:09:34
 * @description
 * 参数说明
 * 200 成功
 * 404 不存在
 * 500 错误
 */
@Getter
@Setter
public class ResultUtil {
	private int code;
//	private String status;
	private String msg;
	private Object data;
	
	public ResultUtil(int code, String msg) {
		this.code=code;
		this.msg=msg;
		this.data = null;
	}
	public ResultUtil(int code, String msg, Object data) {
		this.code=code;
		this.msg=msg;
		this.data=data;
	}
}
